describe("Tests of basic math functions", function() {
    it("Adding 1 should work", function() {
        var foo = 0;
        foo += 1;
        expect(foo).toEqual(1);
    });

    it("Subtracting 1 should work", function () {
        var foo = 0;
        foo -= 1;
        expect(foo).toEqual(-1);
    });

    it("UI Test: Add Button Hides Listing", function(){
    // click the button for showing the add button
    $('#showFormButton').click();
    // expect that the add form is not hidden
    var tmp: any;
    tmp = $("#addElement").attr("style");
    expect(tmp.indexOf("display: none;")).toEqual(-1);
    // expect tha tthe element listing is hidden
    tmp = $("#showElements").attr("style");
    expect(tmp.indexOf("display: none;")).toEqual(0);
    // reset the UI, so we don't mess up the next test
    $('#addCancel').click();
    });

});

var describe: any;
var it: any;
var expect: any;
